import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {EventData} from 'data/observable';
import {Router} from '@angular/router';
import {GameService} from '../../services/game.service';
import {Page} from 'ui/page';
import { topmost } from "tns-core-modules/ui/frame";
import { isIOS } from "tns-core-modules/platform";


@Component({
    moduleId: module.id,
    selector: 'board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.css'],
})

export class BoardComponent implements OnInit {

    private player: string;
    public activePlayer: string;
    private scores: any;
    private numberOfTurns: number;


    /*
    stuff im working on
    */

    //public Score = new Score(); 
    // public xCount: number = 0; 
    // public oCount: number = 0; 
    // @Output() public playerXwins: EventEmitter<Number> = new EventEmitter();
    // @Output() public playerOwins: EventEmitter<Number> = new EventEmitter();

    constructor(private gameService: GameService, private router: Router, private page: Page) {
        page.actionBarHidden = true;
    }

    ngOnInit() {
        this.player = 'x';
        this.activePlayer = this.showPlayerName(this.player);
        this.scores = {x: 0, o: 0};
        this.numberOfTurns = 0;
    }

    public setTile(args: EventData) {
        const button = args.object; 
        button.set('text', this.player.toUpperCase());
        button.set('isEnabled', false); 

        this.numberOfTurns++;
        this.scores[this.player] = this.scores[this.player] + parseInt(button.get('score'));
        this.checkPlayersMove();

        this.player = this.player === 'o' ? "x" : 'o';
        this.activePlayer = this.showPlayerName(this.player);

        this.gameService.clickSound();
    }


    
    private showPlayerName(player: string): string {
        return 'Turn - ' + player;
    }

    //turn number set for max number of actual tic tac toe moves
    private checkPlayersMove() {
        if (this.gameService.checkWins(this.scores[this.player])) {
            // if (this.player == 'x'){this.playerXwins.next(this.xCount++) }
            // else{this.playerOwins.next(this.oCount++)}
            this.router.navigate(['gameresult/' + this.player.toUpperCase()]);
        }else if (this.numberOfTurns === 9) {
            this.router.navigate(['gameresult/tie']);
        }
    }

    //this function reloads the board screen and will re-init everything. 
    public restartGame() {
        this.router.navigate(['gameresult/restart']);

        //this.router.navigate(['board']);
        
    }
}
