import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, NgZone} from '@angular/core';
import {Router} from '@angular/router';
import {ActivatedRoute} from '@angular/router';
import {Page} from 'ui/page';
import { setInterval, clearInterval, setTimeout } from "tns-core-modules/timer";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    moduleId: module.id,
    selector: 'gameresult',
    templateUrl: './gameresult.html',
    styleUrls: ['./gameresult.css'],
    changeDetection: ChangeDetectionStrategy.Default
})

export class GameResultComponent implements OnInit {
    public result: string;
    public id: string;
    public isReset: boolean = false;
    public xWins = appSettings.getNumber("playerXwins");
    public oWins = appSettings.getNumber("playerOwins");

    constructor(private router: Router, private zone: NgZone, private cdRef: ChangeDetectorRef, private route: ActivatedRoute, private page: Page) {
        page.actionBarHidden = true;
    }


    ngOnInit(): void {
        //this is just an easy less time consuming way for me to reset or show a win
        if(appSettings.getNumber("playerXwins") === undefined){
            appSettings.setNumber("playerXwins", 0);
        }
        if(appSettings.getNumber("playerOwins") === undefined){
            appSettings.setNumber("playerOwins", 0);
        }
        this.id = this.route.snapshot.params['id'];
        this.result = this.getResult();
        this.refreshVals(); 
        
    }


    //handles the logic of showing a win or reset
    public getResult(): string {
        if (this.id === 'restart'){
            this.isReset = true;
            setTimeout(() => {
                this.router.navigate(['board']);
            }, 1000);
            return this.result = 'Resetting Board';
        }
        else if (this.id === 'tie'){
            return this.result = "Boooooo Tie Game";
            }
        else { 
            if (this.id === 'X'){
                appSettings.setNumber("playerXwins", this.incrementSum(appSettings.getNumber('playerXwins')));
            }
            if (this.id === 'O') {
                appSettings.setNumber("playerOwins", this.incrementSum(appSettings.getNumber('playerOwins')));
            }
            return this.result = 'Player ' + this.id + ' won the game!'; } 
    }

    public incrementSum(num): any{
        let add = 1; 
        return num+add;
    }

    public refreshVals(){
        this.xWins = appSettings.getNumber("playerXwins");
        this.oWins = appSettings.getNumber("playerOwins");
    }

    //this offers a chance to reset the game from a tie or victory screen
    public gameRestart() {
        this.router.navigate(['board']);
    }
}
