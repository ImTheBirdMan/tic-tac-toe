import {Injectable} from '@angular/core';

let sound = require('nativescript-sound');

@Injectable()
export class GameService {

    private soundFile: any = sound.create('~/assets/sound/click.mp3');

    public checkWins(playerScore: number): boolean {

        const possibleWins = [7, 56, 448, 73, 146, 292, 273, 84];

        for (let i = 0; i < possibleWins.length; i++) {
            if ((possibleWins[i] & playerScore) === possibleWins[i]) {
                return true;
            }
        }
        return false;
    }

    /*
     * Play sound when the players do their moves.
     */
    public clickSound(): void {
        this.soundFile.reset();
        this.soundFile.play();
    }
}
