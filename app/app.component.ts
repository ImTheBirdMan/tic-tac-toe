
import {Component} from '@angular/core';
import { topmost } from "ui/frame";
@Component({
    selector: 'app-root',
    template: '<page-router-outlet></page-router-outlet>',
})
export class AppComponent {
    
}
