Please install Nativescript with Angular and Typescript. 

Please use a Mac otherwise a pc with VNC capabily would be needed to run the ios simulators.

__Steps__: 

1 - install Node.js 

2 - install Nativescript by running <npm install -g nativescript@5.4.0>
    
    5.4 is not the latest. 6.0 introduced several errors I am avoiding by staying on 5.4.0

3 - clone the repo and navigate inside it.

4 - run <tns doctor> to make sure everything is installed correctly.

5 - Run on iOS iphone X/Xs/Xr simulator using <tns run ios> after cloning the repo and installing nativescript. 

    => The default sim is set to iphone 6. 
    => To get the X simulator running, after using the <tns run ios> command, please select 'Hardware' then pick a new ios 12 device: iPhone X        

I used a mathematical solution to building the tic tac toe logic. I 've created a tic tac toe app in the past based on
an array system of 

    [00][01][02]
    [10][11][12]
    [20][21][22]

But I figured there was probably an easier way of doing it and as usual there will always be someone with a simpler solution to problem 
in software engineering. 

I saw that a quick score system could be used to tally all X's or O's and I adopted this technique in the app. 
Being that this is less logic intensive the app was created very quickly :D. 

Logic for a math based tic tac toe ++++



    1   2    4
    8   16   32
    64  128  256
    ============



Win Sums = [7 56 73 84 146 273 292 448]

Project is built on top of vanilla project structure from a NativeScript tutorial so some files and methods were already here. 